$.validator.addMethod("requiredemail", function (value, element) {
    pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return pattern.test(value.slice(1));
}, "Не валид!");

$(document).ready(function() {

});


var progress = true;
function ls_ajax_test(data) {
    $('.progress-bar').addClass('progress-bar-striped');
    $('.progress-bar').addClass('active');

    var myVar = setInterval(function() {
        ls_ajax_progress(data);
    }, 1000);

    $.ajax({
        type: 'POST',
        url: '/?parser',
        data: data,
        dataType: 'json',
        success: function(data) {
            clearInterval(myVar);
            console.log('DONE');
            console.log('DONE DATA');
            progress = false;
            console.log(data);
            setProgress(data.i);
            $('.progress-bar').removeClass('progress-bar-striped');
            $('.progress-bar').removeClass('active');
            $('#result').val(data.result);
        },
    });

    return false;
}
function ls_ajax_progress(data) {
    $.ajax({
        type: 'POST',
        url: '/?progress',
        data: data,
        dataType: 'json',
        success: function(data) {
            if(progress) {
                console.log('DATA');
                console.log(data);

                setProgress(data.i);
            }
        },
    });

    return false;
}
function uniqueid(){
    var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
    do {
        var ascicode=Math.floor((Math.random()*42)+48);
        if (ascicode<58 || ascicode>64){
            idstr+=String.fromCharCode(ascicode);
        }
    } while (idstr.length<32);

    return (idstr);
}

function setProgress(value) {
    $('.progress-bar').text(value + ' %');
    $('.progress-bar').css('width', value + '%');
}