/**
 * Created by Anton on 04.06.2017.
 */
$(document).ready(function() {
    var progress = true;
    function ls_ajax_test(data) {
        $('.progress-bar').addClass('progress-bar-striped');
        $('.progress-bar').addClass('active');

        var myVar = setInterval(function() {
            ls_ajax_progress(data);
        }, 1000);

        $.ajax({
            type: 'POST',
            url: '/?parser',
            data: data,
            dataType: 'json',
            success: function(data) {
                clearInterval(myVar);
                console.log('DONE');
                console.log('DONE DATA');
                progress = false;
                console.log(data);
                setProgress(data.i);
                $('.progress-bar').removeClass('progress-bar-striped');
                $('.progress-bar').removeClass('active');
                $('#result').val(data.result);
            },
        });

        return false;
    }
    function ls_ajax_progress(data) {
        $.ajax({
            type: 'POST',
            url: '/?progress',
            data: data,
            dataType: 'json',
            success: function(data) {
                if(progress) {
                    console.log('DATA');
                    console.log(data);

                    setProgress(data.i);
                }
            },
        });

        return false;
    }
    function uniqueid(){
        var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
        do {
            var ascicode=Math.floor((Math.random()*42)+48);
            if (ascicode<58 || ascicode>64){
                idstr+=String.fromCharCode(ascicode);
            }
        } while (idstr.length<32);

        return (idstr);
    }

    function setProgress(value) {
        $('.progress-bar').text(value + ' %');
        $('.progress-bar').css('width', value + '%');
    }

    $('[name="token"]').val(uniqueid());
    $('[js-users-parse]').submit(function() {
        progress = true;
        setProgress(0);
        $('[name="token"]').val(uniqueid());
        $('.progress-bar').addClass('progress-striped');
        $('.progress-bar').addClass('active');
        ls_ajax_test($(this).serialize());
        return false;
    });
});