/**
 * Created by admin on 13.05.2017.
 */

$(document).ready(function() {
    $('[name="token"]').val(uniqueid());

    $('[js-openmess-parse]').submit(function() {
        progress = true;
        setProgress(0);
        $('[name="token"]').val(uniqueid());
        $('.progress-bar').addClass('progress-striped');
        $('.progress-bar').addClass('active');
        ls_ajax_test($(this).serialize());
        return false;
    });
});