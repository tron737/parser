/**
 * Created by admin on 12.05.2017.
 */
$(document).ready(function() {
    $('[js-authorization-form]').validate({
        rules: {
            'auth[email]': {
                requiredemail: true,
            },
            'auth[password]': {
                required: true,
            }
        },
        submitHandler: function () {
            $.ajax({
                url: location.pathname,
                type: 'POST',
                dataType: 'json',
                data: $('[js-authorization-form]').serialize(),
                success: function (dataRes) {
                    var $resultBloc = $('[js-authorization-result]');
                    $resultBloc.attr('class', 'alert alert-' + dataRes.status)
                    $resultBloc.text(dataRes.message)
                    $resultBloc.show();
                    if(dataRes.status == 'success') {
                        setTimeout(function() {
                            location.reload()
                        }, 1500);
                    }
                }
            });
        }
    });
    $('[js-registration-form]').validate({
        rules: {
            'reg[email]': {
                requiredemail: true,
            },
            'reg[password]': {
                required: true,
                equalTo: '#repeatpassword'
            },
            'reg[repeatpassword]': {
                required: true,
                equalTo: '#password'
            },
        },
        submitHandler: function () {
            $.ajax({
                url: location.pathname,
                type: 'POST',
                dataType: 'json',
                data: $('[js-registration-form]').serialize(),
                success: function (dataRes) {
                    var $resultBloc = $('[js-registration-result]');
                    $resultBloc.attr('class', 'alert alert-' + dataRes.status)
                    $resultBloc.text(dataRes.message)
                    $resultBloc.show();
                    if(dataRes.status == 'success') {
                        setTimeout(function() {
                            location.reload()
                        }, 1500);
                    }
                }
            });
        }
    });
});