<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.02.2017
 * Time: 18:11
 */

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/Models/runModel.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Controllers/runController.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Application/Application.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/DataBase/db.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Request/Request.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Auth/Authorization.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Helpers/data.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Helpers/Validation/Validation.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Parser.class.php';


new Application(substr($_SERVER['REQUEST_URI'], 2));