<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.02.2017
 * Time: 18:14
 */
class Application extends runController
{
    public function __construct($path)
    {
        $name = explode('/', $path);
        $args = array();

        foreach ($name as $item) {
            if(!empty($item) && $item != $name[0]) {
                $args[] = $item;
            }
        }

        $this->runModel($name[0], $args);
        $this->runController(explode('/', $path), $args);
//        var_dump($this);
    }
}