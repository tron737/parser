<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.02.2017
 * Time: 19:34
 */
class runController extends runModel
{
    public $tpl;
    public $tplAuth = 'auth';
    public $activeJsScript;
    public $jsPath = '/static/js/';
    public $activeTpl;
    public $pathUri;
    public $activeFile;
    public $defaultTpl = 'home.php';
    public $mainTemplate = 'mainTemplate.php';
    public $errorPage = '404.php';
    public $TemplateDirectory = '/Template/';
    public $title = array(
        'main' => 'Подбор аудитории для таргетинга',
    );
    public $blackHtml = array('parser', 'progress');

    public function runController($path) {
        $tmpPath = [];

        foreach ($path as $item) {
            if(!empty($item)) {
                $tmpPath[] = $item;
            }
        }
        $path = $tmpPath;

        if(!isset($path[1]) && !is_string($path[1])) {
            $templateName = $path[0];

            $this->activeFile = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->mainTemplate;
            $this->activeTpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $templateName . '.php';
        } else {
            $templateName = $path[1];

            $this->activeFile = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->mainTemplate;
            $this->activeTpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $path[0] . '/' . $templateName . '.php';

            $this->TemplateDirectory = $this->TemplateDirectory . $path[0] . '/';

            $this->jsPath = $this->jsPath . $path[0] . '/';
        }

        if(empty($templateName) || !array_key_exists($templateName, $this->title)) {
            $this->title = $this->title['main'];
        } else {
            $this->title = $this->title[$templateName];
        }

        if(!empty($templateName)
            && file_exists($this->activeTpl)
            && is_readable($this->activeTpl)) {
            if($this->mainModel->isAuth) {
                $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $templateName . '.php';
                $tmpJsName = $templateName;
            } else {
                $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->tplAuth . '.php';
                $tmpJsName = $this->tplAuth;
            }
            $jsScript = $this->jsPath . $tmpJsName . '.js';
            if(file_exists($_SERVER['DOCUMENT_ROOT'] . $jsScript) && is_readable($_SERVER['DOCUMENT_ROOT'] . $jsScript)) {
                $this->activeJsScript = $jsScript;
            }
        } elseif(empty($templateName)) {
            if($this->mainModel->isAuth) {
                $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->defaultTpl;
                $tmpJsName = $templateName;
            } else {
                $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->tplAuth . '.php';
                $tmpJsName = $this->tplAuth;
            }

            $jsScript = $this->jsPath . $tmpJsName . '.js';
            if(file_exists($_SERVER['DOCUMENT_ROOT'] . $jsScript) && is_readable($_SERVER['DOCUMENT_ROOT'] . $jsScript)) {
                $this->activeJsScript = $jsScript;
            }
        } else {
            $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->errorPage;
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->blackHtml[] = $templateName;
        }
        if(!in_array($templateName, $this->blackHtml)) {
            require_once strval($this->activeFile);
        }
    }
}