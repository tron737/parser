<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.05.2017
 * Time: 14:19
 */
//class Authorization
//{
//    private $client_id = '6060138';
//    private $client_secret = '3esBKrzciwrKDFhXpQf3';
//    private $redirect_uri = 'http://pvk/';
//    private $url = 'http://oauth.vk.com/authorize';
//    public $authUrl = null;
//    public $db;
//
//    public function __construct()
//    {
//        $this->db = new db();
//        if (isset($_REQUEST['code'])) {
//            $code = htmlspecialchars(trim($_REQUEST['code']));
//
//            $params = array(
//                'client_id' => $this->client_id,
//                'client_secret' => $this->client_secret,
//                'code' => $code,
//                'redirect_uri' => $this->redirect_uri
//            );
//            $token = json_decode(Request::get('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))));
//            if (isset($token->access_token)) {
//                $this->token = $token->access_token;
//                self::setAccessKey($token->access_token);
//                header('Location: /');
//            }
//
//        }
//
//        $params = array(
//            'client_id' => $this->client_id,
//            'redirect_uri' => $this->redirect_uri,
//            'response_type' => 'code'
//        );
//        $this->authUrl = $this->url . '?' . urldecode(http_build_query($params));
//    }
//
//    public function checkAuthorization() {
//        $res = isset($_SESSION['token']);
//        return $res;
//    }
//
//    public function setAccessKey($token)
//    {
//        $_SESSION['token'] = $token;
//        setcookie('token', $token);
//    }
//
////    public $db;
////    public $admin = false;
////
////    public function __construct()
////    {
////        $this->db = new db();
////    }
////
////    public function Registration($email, $password, $repeatPassword) {
////        if($password != $repeatPassword) {
////            return false;
////        }
////        if(!is_null($this->db->checkEmail($email))) {
////            return false;
////        }
////        $this->db->addNewUser($email, md5(md5($password)));
////        self::setAccessKey($email);
////        return true;
////    }
////
////    public function checkAuthData($email, $password) {
////        $result = $this->db->checkAuthData($email, md5(md5($password)));
////        if(is_array($result)) {
////            self::setAccessKey($result['email']);
////            return true;
////        } else {
////            return false;
////        }
////    }
////
////    public function setAccessKey($email) {
////        $key = self::generationAccessKey();
////        $this->db->setAuthAccessKey($key);
////
////        $_SESSION['key'] = $key;
////        setcookie('key', $key);
////
////        $_SESSION['email'] = $email;
////        setcookie('email', $email);
////    }
////    private function generationAccessKey() {
////        return md5(uniqid('', true));
////    }
//}

class Authorization
{
    private $clientId = 6060138;
    private $clientSecret = '3esBKrzciwrKDFhXpQf3';
    private $redirectUri = 'http://pvk/';
    private $url = 'http://oauth.vk.com/authorize';
    private $OAuthUrl = 'https://oauth.vk.com/access_token';
    private $urlUsersGet = 'https://api.vk.com/method/users.get';
    private $code = '';
    private $resultArr;
    public $personalInformation;

    public function __construct() {
        if($this->isLocalhost()) {
            $this->clientId = 6060138;
            $this->clientSecret = '3esBKrzciwrKDFhXpQf3';
            $this->redirectUri = 'http://pvk/';
        } else {
            $this->clientId = 5973454;
            $this->clientSecret = 'w3KB9Em4etQeYnMwljCV';
            $this->redirectUri = 'http://188.120.244.239/?home/';
        }


    }

    private function isLocalhost() {
        return $_SERVER['HTTP_HOST'] == 'pvk';
    }

    public function generationAuthUrl() {
        $params = array(
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'response_type' => 'code'
        );

        return $this->url . '?' . urldecode(http_build_query($params));
    }

    public function getToken($code) {
        if(!empty($code)) {
            $params = array(
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'code' => $code,
                'redirect_uri' => $this->redirectUri,
            );

            $url = $this->OAuthUrl . '?' . urldecode(http_build_query($params));

            $result = Request::get($url);

            $this->code = $code;

            $this->resultArr = json_decode($result, true);

            if(isset($this->resultArr['access_token'])) {
                $_SESSION['token'] = $this->resultArr['access_token'];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getPersonalInformation() {
        $params = array(
            'uids' => $this->resultArr['user_id'],
            'fields' => 'uid,photo_100,first_name,last_name',
            'access_token' => $this->resultArr['access_token'],
        );

        $url = $this->urlUsersGet . '?' . urldecode(http_build_query($params));
        $userInfo = json_decode(Request::get($url), true);
        $this->personalInformation = $userInfo['response'][0];
        if(!empty($this->personalInformation)) {
            return true;
        } else {
            return false;
        }
    }
    public function saveToSession() {
        $_SESSION['user'] = $this->personalInformation;
        $_SESSION['token'];
    }

    public function checkAuthorization() {
        return isset($_SESSION['user']);
    }
}