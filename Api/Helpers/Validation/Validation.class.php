<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.05.2017
 * Time: 14:34
 */

namespace Api\Helpers\Validation;

use Api\Helpers\data;

class Validation
{
    public static function checkEmail($email) {
        $email = data::checkSecureString($email);
        return filter_var($email, FILTER_VALIDATE_EMAIL) ? $email : false;
    }
    public static function checkPassword($password) {
        $password = data::checkSecureString($password);
        return !empty($password) ? $password : false;
    }
}