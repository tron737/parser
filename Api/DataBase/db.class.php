<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.07.17
 * Time: 18:17
 */

namespace Api\DataBase;

class db
{
    public function __construct()
    {
        $this->mysqli = new mysqli("localhost", "root", "", "vkparse");
        $this->mysqli->set_charset('utf8');
    }

    private function query($sql)
    {
        $args = func_get_args();

        $sql = array_shift($args);
        $link = $this->mysqli;

        $args = array_map(function ($param) use ($link) {
            return "'" . $link->escape_string($param) . "'";
        }, $args);

        $sql = str_replace(array('%', '?'), array('%%', '%s'), $sql);

        array_unshift($args, $sql);

        $sql = call_user_func_array('sprintf', $args);


        $this->last = $this->mysqli->query($sql);
        if ($this->last === false) throw new Exception('Database error: ' . $this->mysqli->error);

        return $this->last;
    }

    private function assoc()
    {
        return $this->last->fetch_assoc();
    }

    private function all()
    {
//        return $this->last->fetch_all();
        $result = array();
        while ($row = $this->last->fetch_assoc()) $result[] = $row;
        return $result;
    }
}