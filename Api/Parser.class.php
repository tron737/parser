<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.05.2017
 * Time: 0:11
 */
class Parser
{
    public $url = 'https://api.vk.com/method/';
    public $uid;

    public function __construct($uid) {
        $this->uid = $uid;
    }

    public function getGroupFollower($id, $token, $inGroup = 1) {
        $this->url = $this->url . 'groups.getMembers?';
        $ids = explode("\r\n", $id);
        $ids = array_unique($ids);
        $params = array();
        foreach($ids as $item) {
            $params[] = [
                'group_id' => htmlspecialchars(trim($item)),
                'sort' => 'id_asc',
                'offset' => 0,
                'count' => 1000,
            ];
        }

        $users = array();
        $sumCount = 0;
        for($i = 0; $i < count($params); ++$i) {
            $res = json_decode(Request::get($this->url . http_build_query($params[$i])));
            $res = $res->response;
            $counter = $res->count;
            $users = array_merge($users, $res->users);
            $sumCount += $counter;

            $params[$i]['counter'] = $counter;
        }

        if($sumCount < 1000) {
            session_start();
            $_SESSION[$token]['i'] = 100;
            session_write_close();
        }

        $newI = 0;
        $tmpNewI = 0;
        for($j = 0; $j < count($params); ++$j) {
            for($i = 1000; $i <= intval($params[$j]['counter']); $i += 1000) {
                session_start();

                $newI = $i;

                $_SESSION[$token]['i'] = round(($tmpNewI + $newI) / $sumCount * 100, 2);
                session_write_close();

                $params[$j]['offset'] = $i;
                $result = json_decode(Request::get($this->url . http_build_query($params[$j])));
                $result = $result->response;
                $users = array_merge($users, $result->users);
            }
            $tmpNewI = $tmpNewI + $newI;
        }

        if($inGroup != 1) {
            $newUser = array_count_values($users);

            $tmpUser = array();

            foreach ($newUser as $key => $count) {
                if($count === $inGroup) {
                    $tmpUser[] = $key;
                }
            }

            $users = $tmpUser;
        }

        $users = array_unique($users);

        $_SESSION[$token]['i'] = 100;

        $sessionData = $_SESSION[$token];

        unset($_SESSION[$token]);

        return json_encode(
            array_merge(
                $sessionData,
                array(
                    'result' => implode("\r\n", $users),
                    'count' => count($users),
                    'token' => $token,
                )
            )
        );
    }

    public function getSpecFind($city, $age, $career) {
        $this->url .= 'users.search?';
        $params = [
            'country' => $city,
            'age_from' => $age,
            'age_to' => $age,
            'count' => 1000,
            'fields' => 'career',
            'access_token' => $_SESSION['token']
        ];
        $res = json_decode(Request::get($this->url . http_build_query($params)));

        session_start();
        $_SESSION[$this->uid]['i'] = 100;
        session_write_close();

        $result = array();

        foreach($res->response as $item) {
            if(is_object($item) && isset($item->career)) {
                $result[] = $item->uid . ' ' . $item->first_name . ' ' . $item->last_name;
            }
        }

        $sessionData = $_SESSION[$this->uid];

        return json_encode(
            array_merge(
                $sessionData,
                array(
                    'result' => implode("\r\n", $result),
                    'count' => count($result),
                    'token' => $this->uid,
                )
            )
        );
    }
}