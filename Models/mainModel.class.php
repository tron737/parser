<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.02.2017
 * Time: 23:32
 */

use Api\Auth\Authorization;

class mainModel
{
    public $test = 'erg';
    public $isAuth = false;
    public $isAdmin = false;

    public function __construct() {
        session_start();
        $auth = new Authorization();
        $this->isAuth = $auth->checkAuthorization();

        $this->displayUrl = $auth->generationAuthUrl();
        if(isset($_REQUEST['code'])) {
            $auth->getToken($_REQUEST['code']);
            $getAuth = $auth->getPersonalInformation();
            if($getAuth) {
                $auth->saveToSession();
                header('Location: /');
            }
        }

    }

}