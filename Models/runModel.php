<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.02.2017
 * Time: 23:11
 */
class runModel
{
    public $modelDir = '/Models/';
    public $nameMainModel = 'mainModel';
    public $defaultModel = 'homeModel'; //name model in main page
    public $authModel = 'authModel';
    public $mainModel;
    public $model;
    public function runModel($nameModel) {
        $args = func_get_args();
        //check model file

        $nameModel = array_shift($args);
        $args = array_shift($args);

        if(is_numeric(end($args))) {
            $page = intval(end($args));
        }

        require_once $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $this->nameMainModel . '.class.php';
        $this->mainModel = new $this->nameMainModel();

        if(!empty($nameModel)) {
            if(isset($args[0])) {
                $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $nameModel . '/' . $args[0] . 'Model' . '.class.php';
                $runModel = $args[0]  . 'Model';
            } else {
                $nameModel = $nameModel . 'Model';
                $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $nameModel . '.class.php';
                $runModel = $nameModel;
            }
        } else {
            $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $this->defaultModel . '.class.php';
            $runModel = $this->defaultModel;
        }

        if(!$this->mainModel->isAuth) {
            $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $this->authModel . '.class.php';
            $runModel = $this->authModel;
        }

        if (file_exists($nameModelPath) && is_readable($nameModelPath)) {
            require_once $nameModelPath;
            if(isset($page)) {
                $this->model = new $runModel($page);
            } else {
                $this->model = new $runModel();
            }
        }
    }
}